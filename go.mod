module gitee.com/VocaloidAt/gohide

go 1.14

require (
	gitee.com/VocaloidAt/filelasting v1.0.3
	github.com/getlantern/filepersist v0.0.0-20160317154340-c5f0cd24e799
	github.com/getlantern/golog v0.0.0-20190830074920-4ef2e798c2d7 // indirect
)
