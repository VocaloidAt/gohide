# gohide

#### 介绍

提供了一种在Windows gui上执行内容，而又不会生成可见的shell窗口与命令行的方法。

```
当使用 go build -ldflags="-H windowsgui" 编译程序以构建gui模式的Windows可执行文件时。
使用外壳套用的方式，让外壳运行命令而不是当前窗口使用,

在linux,mac的平台上，它仅传递给exec.Command()。运行！
使用Visual Basic代码从http://www.robvanderwoude.com/files/runnhide_vbs运行。
```



#### 使用说明

1.  使用go mod，引入依赖：`gitee.com/VocaloidAt/gohide`
2.  直接使用go get 引入依赖：`gitee.com/VocaloidAt/gohide`

#### 使用方法

###### go mod 方式-

* 在go mod中引入依赖 当前版本为v1.0.3，根据码云标签运行最新版即可

![image-20200724150127445](README.assets/image-20200724150127445.png)

###### go get方式-

* 直接在命令行输入`gitee.com/VocaloidAt/gohide`即可

#### 使用示例

1.利用go test执行测试文件`gohide_test.go`即可

2.项目引入依赖使用示例：

![image-20200724150518892](README.assets/image-20200724150518892.png)

