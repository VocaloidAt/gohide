/**
	提供了一种在Windows gui上执行内容，而又不会生成可见的shell窗口与命令行的方法。


	当使用 go build -ldflags="-H windowsgui" 编译程序以构建gui模式的Windows可执行文件时，这一点很重要。
	使用外壳套用的方式，让外壳运行命令而不是当前窗口使用,

	在linux,mac的平台上，它仅传递给exec.Command()。运行！
	使用Visual Basic代码从http://www.robvanderwoude.com/files/runnhide_vbs运行。
 */
package gohide

import (
	"os/exec"
)

func Command(name string, arg ...string) *exec.Cmd {
	return command(name, arg...)
}
