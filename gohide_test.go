/**
	test
 */
package gohide

import (
	"testing"
)

func TestOpenURL(t *testing.T) {
	cmd := Command("cmd", "/C", "start", "", "https://www.sogou.com")
	out, err := cmd.CombinedOutput()
	t.Logf("Error?: %v", err)
	t.Log(out)
}
